import axios from 'axios';

export default {
  getAll: async () => {
    let res = await axios.get(`/api/product`);
    return res.data || [];
  },

  delete: async (id) => {
    let res = await axios.delete(`/api/product/${id}`);
    return res.data || [];
  },

  put: async (name, description) => {
    let res = await axios.put(`/api/product`, {
      name, description
    });
    return res.data || [];
  },
}