import React, { useState, useEffect } from "react";

// SERVICES
import productService from './services/productService';

function App() {
  const [products, setproducts] = useState(null);

  useEffect(() => {
    if(!products) {
      getProducts();
    }
  })

  const getProducts = async () => {
    let res = await productService.getAll();
    setproducts(res);
  }

  const deleteProduct = async (e) => {
    const id = e.target.dataset.keyid;
    if(id){
      let res = await productService.delete(id);
      setproducts(res);
    }
  }

  const addSale = async (e, idNameField, idDescField) => {
    e.preventDefault();
    const name = document.getElementById(idNameField).value;
    const description = document.getElementById(idDescField).value;
    if (name){
      let res = await productService.put(name, description);
      setproducts(res);
    }
  }

  const renderProduct = product => {
    return (
      <li key={product.id} className="list__item product">
        <h3 className="product__name">{product.name}</h3>
        <p className="product__description">{product.description}</p>
        <button data-keyId={product.id}>Delete</button>
      </li>
    );
  };

  return (
    <div className="App">
      <ul className="list" onClick={(e) => deleteProduct(e)}>
        {(products && products.length > 0) ? (
          products.map(product => renderProduct(product))
        ) : (
          <p>No products found</p>
        )}
      </ul>
      <form>
          <label for="name">Name:</label>
          <input type="text" id='name' placeholder="12"></input>
          <label for="description">Description:</label>
          <input type="text" id='description'></input>
          <input type="submit" onClick={(e) => addSale(e, 'name', 'description')}/>
      </form>
    </div>
  );
}

export default App;
