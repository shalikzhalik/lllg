const mysql = require('mysql2/promise');

async function connect(){
    const connection = await mysql.createConnection({
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        user: process.env.DB_USER,
        database: process.env.DB_NAME,
        password: process.env.DB_PASS
      });
      
      return connection;
}


module.exports =  { connection: connect() } ;
      



